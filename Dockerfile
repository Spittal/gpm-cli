FROM node:alpine

RUN apk add yarn

ADD package.json yarn.lock /
RUN yarn

ADD ./ /
RUN yarn build

CMD node dist/index.js
