import PlayMusic from './PlayMusic/PlayMusic';

async function main () {
  const pm: PlayMusic = new PlayMusic('replace_with_email', 'replace_with_password');
  await pm.init();
  await pm.getLibrary();
}

main();
