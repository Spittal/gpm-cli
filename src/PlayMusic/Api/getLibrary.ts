import axios, { AxiosRequestConfig } from 'axios';

export async function getLibrary (token: string, options: GetLibraryOptions = { limit: 1000 }): Promise<any> {
  const opts: AxiosRequestConfig = {
    method: 'POST',
    url: 'https://mclients.googleapis.com/sj/v2.5/trackfeed',
    params: {
      'alt': 'json',
      'include-tracks': 'true'
    },
    headers: {
      'Authorization': `GoogleLogin auth=${token}`,
      'Content-Type': `application/json`,
    }
  };
  
  return axios.request(opts).then(res => res.data);
};

export interface GetLibraryOptions {
  limit: number
};
