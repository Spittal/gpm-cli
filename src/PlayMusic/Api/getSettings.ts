import axios from 'axios';

export async function getSettings (token: string): Promise<any> {
  return axios.request({
    method: 'POST',
    url: 'https://play.google.com/music/services/fetchsettings?u=0',
    headers: {
      Authorization: `GoogleLogin auth=${token}`,
      'Content-Type': `application/json`
    }
  }).then(res => res.data);
};

