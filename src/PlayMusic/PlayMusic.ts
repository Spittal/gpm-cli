import AuthService from './Auth/AuthService';
import {
  getSettings,
  getLibrary
} from './Api';

export default class PlayMusic {
  private token: string;

  constructor (private email: string, private password: string) {}

  public async init (): Promise<any> {
    this.token = await AuthService.getToken(this.email, this.password);
  }

  public async getSettings (): Promise<any> {
    return getSettings(this.token);
  }
  
  public async getLibrary (): Promise<any> {
    return getLibrary(this.token);
  }
}
