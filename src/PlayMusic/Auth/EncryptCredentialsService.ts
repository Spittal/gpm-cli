import crypto from 'crypto';
import getPem from 'rsa-pem-from-mod-exp';

/*
* Encrypt the username/password for use in `EncryptedPasswd`.
* refs:
* - https://github.com/yeriomin/play-store-api/blob/master/src/main/java/com/github/yeriomin/playstoreapi/PasswordEncrypter.java
* - https://github.com/subtletech/google_play_store_password_encrypter/blob/master/google_play_store_password_encrypter.rb
*
*	We first convert the public key to RSA PEM format which is used
*	throughout node's standard library.
*
*	The result is something like the below
*	-----------------------------------------------------------------------------
*	|00|4 bytes of sha1(publicKey)|rsaEncrypt(publicKeyPem, "login\x00password")|
*	-----------------------------------------------------------------------------
*	The result is then base64 URL-safe encoded and can be used as the
*	`EncryptedPasswd`
*/
class EncryptCredentialsService {
  private GOOGLE_DEFAULT_PUBLIC_KEY: string = 'AAAAgMom/1a/v0lblO2Ubrt60J2gcuXSljGFQXgcyZWveWLEwo6prwgi3iJIZdodyhKZQrNWp5nKJ3srRXcUW+F1BD3baEVGcmEgqaLZUNBjm057pKRI16kB0YppeGx5qIQ5QjKzsR8ETQbKLNWgRY0QRNVz34kMJR3P/LgHax/6rmf5AAAAAwEAAQ==';

	/**
	*	@param {String} email - Google username.
	*	@param {String} password - Google password.
	*	@return {String} `EncryptedPasswd` value.
	*/
  public encryptCredentials(email: string, password: string): string {
    const publicKey: Buffer = Buffer.from(this.GOOGLE_DEFAULT_PUBLIC_KEY, 'base64');
    const digest: Buffer = crypto.createHash('sha1').update(publicKey).digest();
    const signature: Buffer = Buffer.concat([
      Buffer.from('\x00'),
      digest.slice(0, 4)
    ]);

    const encrypted: Buffer = crypto.publicEncrypt(
      this.decompose(publicKey),
      Buffer.from(email + '\u0000' + password)
    );

    const res: Buffer = Buffer.concat([
      signature,
      encrypted
    ]);

    return this.base64EncodeUrlSafe(res);
  }

  private base64EncodeUrlSafe(data: Buffer): string {
    return data.toString('base64').replace(/\+/g, '-').replace(/\//g, '_');
  }

  private decompose(buffer: Buffer): string {
    const buf: Uint8Array = new Uint8Array(buffer);
    const i: number = buffer.readInt32BE(0);
    const mod: Buffer = Buffer.from(buf.buffer, 4, i);
    const j: number = buffer.readInt32BE(i + 4);
    const exp: Buffer = Buffer.from(buf.buffer, i + 8, j);
    return getPem(mod.toString('base64'), exp.toString('base64'));
  }
}

export default new EncryptCredentialsService();
