import axios from 'axios';
import querystring from 'querystring';
import EncryptCredentialsService from './EncryptCredentialsService';

export class AuthService {
  private token: string = null;

  public async getToken(email: string, password: string): Promise<any> {
    const loginPayload = {
      accountType: 'HOSTED_OR_GOOGLE',
      Email: email,
      has_permission: 1,
      add_account: 1,
      EncryptedPasswd: EncryptCredentialsService.encryptCredentials(email, password),
      service: 'ac2dm',
      source: 'android',
      androidId: 'fe80::1c20:5ee4:1cb4:6e7d',
      device_country: 'us',
      operatorCountry: 'us',
      lang: 'us',
      sdk_version: '17'
    };

    const loginData = await axios.request({
      method: "POST",
      url: 'https://android.clients.google.com/auth',
      data: querystring.stringify(loginPayload)
    }).then(res => res.data);
    
    const oAuthPayload: Object = {
      accountType: 'HOSTED_OR_GOOGLE',
      Email: email,
      has_permission: 1,
      EncryptedPasswd: this.parseKeyValues(loginData).Token,
      service: 'sj',
      source: 'android',
      androidId: 'fe80::1c20:5ee4:1cb4:6e7d',
      app: 'com.google.android.music',
      client_sig: '38918a453d07199354f8b19af05ec6562ced5788',
      device_country: 'us',
      operatorCountry: 'us',
      lang: 'us',
      sdk_version: '17'
    };
    
    const oAuthData = await axios.request({
      method: "POST",
      url: 'https://android.clients.google.com/auth',
      data: querystring.stringify(oAuthPayload)
    }).then(res => res.data);    

    return this.parseKeyValues(oAuthData).Auth;
  }

  private parseKeyValues (string: string): { Token: string, Auth: string } {
    const obj: any = {};
    string.split("\n").forEach(line => {
        const pos = line.indexOf("=");
        if(pos > 0) obj[line.substr(0, pos)] = line.substr(pos+1);
    });
    return obj;
};
}

export default new AuthService();
